#include<stdio.h>
#include<string.h>
#include<time.h>     //系统时间
#include<unistd.h>   //linux_time函数
typedef struct user{                //结构体
	    char name[20]; 
		int age;   
		char state[30];   
		int study_time;   
		int play_time;  
}user;

user a[200];
int n = 0;
int i = 0;
int j;

void Welcome()                    //用户界面
{
	    printf("===================GEC record=======================\n");
		printf("                   1. 注册用户                      \n");
		printf("                   2. 记录时长                      \n");
		printf("                   3. 打印信息                      \n");
		printf("                   4. 退出系统                      \n");
		printf("====================================================\n");
		printf("请输入您的选项1-4：");
}


void Scanfinfo()                  //注册用户
{       
		char sign='y';
        //user p[200];
		while(sign!='n'&&sign!='N')
		{
	        printf("\n\n");
		    printf("姓名：");
			scanf("%s",a[i].name);
			if(i != 0)
			{
			  for(int z=0;z<n;z++)
			  {
		         while(strcmp(a[z].name,a[i].name) == 0)
			     {		 					     			 
	                printf("系统中该名字已存在，请重新输入:"); 
				    getchar();
                    scanf("%s",a[i].name);	
			     }
				 break;
			  }
			}
			printf("年龄：");
			scanf(" %d",&a[i].age);	
            while(((int)a[i].age != a[i].age) || a[i].age <=0 )
			{		 					     			 
	             printf("输入的不是正整数，请重新输入:"); 
				 getchar();
                 scanf(" %d",&a[i].age);	
			}
		    printf("情感状态:");
		    scanf("%s",a[i].state);
		    a[i].study_time = 0;
		    a[i].play_time = 0;
		    printf("\n");
		    printf("%s的信息录入成功\t",a[i].name);
		    printf("\n\n");
			i++;
			n = i;
			
			while(1)
		    {
				printf("是否继续注册用户Y/N:"); 
                scanf("\t%c",&sign);
				if(sign!='n'&&sign!='N'&&sign!='y'&&sign!='Y')
				{
					printf("选择错误\n\n");  
					continue;
				}
				else
				{
					break;
				}
			}
			
        }			
}


void Showinfo()                  //打印信息
{
	    int i=0;
		for(i;i<n;i++)
		{	
		   printf("\n\n");
           printf("姓名：%s\n",a[i].name);
		   printf("年龄：%d\n",a[i].age);
		   printf("情感状态：%s\n",a[i].state);
		   printf("学习时间：%d秒\n",a[i].study_time);
		   printf("玩乐时间：%d秒\n",a[i].play_time);
	       printf("\n\n");
		}
}

void Seachinfo()              //查找用户信息信息
{       
        char nstr[20];
		int signal = 0;
         while(1)
         {	
          printf("请输入您的姓名：");
		  scanf("%s",nstr);
		  for(j=0;j<n;j++)
		  {
		    if( strcmp(a[j].name,nstr) == 0 ) //字符串比较  匹配返回0 
		    {
			  printf("找到姓名\n");
			  signal = 1;
			  break;
		    }
		  }
		  if(signal == 0)
		  {
              printf("没有该用户\n");
              continue;
          }
		  else
		  {	  
		    break; 
		  }
		 }		  
}
void Modifyinfo()             //修改信息菜单
{
	    printf("\n\n"); 
	    printf("当前操作用户：%s",a[j].name);
		printf("\n");
		printf("1.修改情感状态\n");
	    printf("2.记录学习时间\n");
		printf("3.记录玩乐时间\n");
		printf("4.返回首页\n");
		printf("请输入您的选项1-4:");
}
void Modifystate()            //修改情感状态
{ 
		char cstr[30];
	    printf("\n");
		printf("请输入新的情感状态：");
		scanf("%s",cstr);
		stpcpy(a[j].state,cstr);
		printf("%s的情感状态修改成功\n",a[j].name);
}

void Studytimeinfo()         //记录学习时间
{
		printf("\n");
		printf("开始学习\n");
		
		char e = '0';
		time_t start_study_time,end_study_time;
		start_study_time = time(NULL);
		printf("计时中。。。\n");
		while(1)
		{
		    printf("如果结束学习，请输入1：");
		    scanf(" %c",&e);
		    if(e == '1')
		    {
			  printf("结束学习\n");
			  end_study_time = time(NULL);
			  a[j].study_time = end_study_time-start_study_time;
			  printf("\n");
		      printf("学习时间：%d秒\n",a[j].study_time);
			  break;
		    }
			else
		    {
			    printf("输入错误\n\n");
			    continue;
		    }
        }
		
}

void Playtimeinfo()           //记录玩乐时间
{

		printf("\n");
	    printf("开始玩乐\n");
	
	    char f = '0';
	    time_t start_play_time,end_play_time;
	    start_play_time = time(NULL);
		printf("计时中。。。\n");
		while(1)
		{
		  printf("如果结束玩乐，请输入1：");
		  scanf(" %c",&f);
	      if(f == '1')
		  {
			printf("结束玩乐\n");
			end_play_time = time(NULL);
			a[j].play_time = end_play_time-start_play_time;
			printf("\n\n");
		    printf("玩乐时间：%d秒\n",a[j].play_time);
			break;
		  }
		  else
		  {
			  printf("输入错误\n\n");
			  continue;
		  }
		}		
}



int main()
{
    int ret;
	
	
    char passwd[10] = "123456";
	
	char str[10] = {0};

	int b = 0;
	int g = 3;

	while(1)
	{	
 	    printf("请输入密码:");//输入密码
	    scanf("%s",str);
        g--;
		ret = strcmp(passwd,str);
		if(ret == 0)
		{
		  while(1){
		    Welcome();//菜单界面
			char n;
			getchar();
            scanf(" %c",&n);
			switch(n)
			{
				case '1': Scanfinfo();//注册用户
				          break;

			    case '2':if(i == 0)
				        {
							printf("没有注册用户,请先注册\n");
							continue;
						}
  				        Seachinfo();//查找用户信息
				        while(1)
					    {
				            Modifyinfo();
							char c;
							getchar();
						    scanf("%c",&c);
						
					        switch(c)
					        {
					           case '1':					
							          Modifystate();//修改情感状态
							          continue;
						
				        	   case '2':
						              Studytimeinfo();//记录学习时间
							          continue;
						
					           case '3':
						              Playtimeinfo();//记录玩乐时间
							          continue;
							  
						       case '4':     
						              printf("\n"); //返回首页
							          break;
							  
						       default: 
							          printf("选择错误!\n");//选择错误
				                      printf("\n\n");
						              continue;		  
						    }
							break;
					   } 
					   break;   
					
				case '3': 
				        if(i == 0)
				        {
							printf("没有注册用户,请先注册\n");
							continue;
						}
				         printf("用户信息如下：\n");
				         Showinfo();//打印信息
                         break;
		 
				case '4': 
				          printf("\n\n");//退出系统
				          printf("see u next time!\n");
                          return 0;	 
				         
                default:
           				 printf("wrong!!!!!!!!!!!!!!!!!!!!!!!");
				         printf("选择错误!\n");//选择错误
				         printf("\n\n");
						 continue;					 					        
			}
		  }
		}
		else 
		{
		  printf("\n"); 
		  printf("密码错误！\n");
		  if( g > 0)
		  { 
			  printf("剩余%d次机会\n",g);
		  }
		  else
		  {
		    printf("byebye!\n");
		    break;
		  }
		}
		
    }
	return 0;
}
